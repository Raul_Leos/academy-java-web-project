package com.softtek.init.controller;

import com.softtek.init.model.Student;
import com.softtek.init.repositories.StudentRepository;
import com.softtek.init.service.StudentService;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

//@ExtendWith(SpringExtension.class)
@SpringBootTest
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
public class StudentRepositoryTest{
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    StudentService studentService;

    //Tests
    //Unitarios
    @Test
    public void notNullDb(){
        assertNotEquals(null,studentRepository.findAll(),"Null list");
    }

    @Test
    public void firstStudent(){
        //setup
        Student correctStudent = new Student(
                1, "CXMG", "08:07:53", "17:36:21", "2019-01-03"
        );
        Student actualStudent;
        //execute
        actualStudent = studentRepository.findAll().get(0);
        //verify
        assertEquals(correctStudent.getName(),actualStudent.getName());
    }

    @Test
    public void lastStudent(){
        //setup
        Student correctStudent = new Student(
                1787, "JFDG1", "05:53:27", "12:29:14", "2019-04-30"
        );
        Student actualStudent;
        //execute
        int size = studentRepository.findAll().size();
        actualStudent = studentRepository.findAll().get(size-1);
        //verify
        assertEquals(correctStudent.getName(),actualStudent.getName());
    }

    //Tests de integracion
    @Test
    public void getAllByMonthNotEmpty(){
        //setup
        List<Student> students;
        //execute
        students = studentService.getAllByMonth("-01-");
        //verify
        assertNotEquals(0,students.size());
    }
}