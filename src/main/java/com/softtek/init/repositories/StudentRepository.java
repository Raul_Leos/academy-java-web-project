package com.softtek.init.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softtek.init.model.Student;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Integer> {
    @Query(
            "SELECT s FROM Student s WHERE s.fecha LIKE %?2% AND s.name = ?1"
    )
    List<Student> getAllByNameAndFecha(String is, String Fecha);

    @Query(
            "SELECT s FROM Student s WHERE s.fecha >= ?2 AND s.fecha <= ?3 AND s.name = ?1"
    )
    List<Student> getUserByEndAndStartDates(String is, String startDate,String endDate);

    @Query(
            "SELECT s FROM Student s WHERE s.fecha LIKE  %?1%"
    )
    List<Student> getAllByMonth(String mes);

    @Query(
            "SELECT s FROM Student s WHERE s.fecha >= ?1 AND s.fecha <= ?2"
    )
    List<Student> getAllBetweenDates(String startDate,String endDate);
}

