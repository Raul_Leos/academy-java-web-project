package com.softtek.init.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.softtek.init.repositories.StudentRepository;

public class Excell {
    public static final String SAMPLE_XLSX_FILE_PATH = "C:\\Users\\raul.leos\\Documents\\Trabajo\\Capacitacion\\Java\\Servlet\\RaulSamuel.ws\\AcademyJavaWebProject\\src\\main\\java\\com\\softtek\\init\\config\\registros-Momentum.xls";

    @Autowired
    private StudentRepository studentRepo;
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Excell testing = new Excell();
		testing.readExcel(SAMPLE_XLSX_FILE_PATH);
	}
	
	public void readExcel(String excelFile) {
		Workbook workbook;
		try {
			workbook = WorkbookFactory.create(new File(excelFile));

	        // Getting the Sheet at index zero
	        Sheet sheet = workbook.getSheetAt(0);

	        // Create a DataFormatter to format and get each cell's value as String
	        DataFormatter dataFormatter = new DataFormatter();
	        int r = 0;
	        int c = 0;
	        List<String> IS = new ArrayList();
	        List<String> fecha = new ArrayList();
	        List<String> cs = new ArrayList();
	        for (Row row: sheet) {
	        	c=0;
	            for(Cell cell: row) {
	            	if(r > 0 && c == 2) {
	            		IS.add(dataFormatter.formatCellValue(cell));
	            	}
	            	if(r > 0 && c == 4) {
	            		fecha.add(dataFormatter.formatCellValue(cell));
	            	}
	            	if(r > 0 && c == 6) {
	            		cs.add(dataFormatter.formatCellValue(cell));
	            	}
	                c++;
	            }
	            r++;
	        }
	        // Closing the workbook
	        workbook.close();
	        List<String> ISCorrect = new ArrayList();
	        List<String> date= new ArrayList();
	        List<String> timeIn= new ArrayList();
	        List<String> timeOut = new ArrayList();
	        //Aqui tendriamos la lista llena
	        for(int i=0; i< IS.size();i++) {
	        	String actual = fecha.get(i);
	        	String anterior;
	        	if( (i+1) < IS.size()) 
	        		 anterior = fecha.get(i+1);
	        	else
	        		break;
	        	String[] dateTimeActual = actual.split(" ");
				String dateActual = dateTimeActual[0];
				String timeActual = dateTimeActual[1];

				
				String[] dateTimeSiguiente = anterior.split(" ");
				String dateSiguiente = dateTimeSiguiente[0];
				String timeSiguiente = dateTimeSiguiente[1];
				
				
				if(!dateActual.equals(dateSiguiente))
					continue;
				if(timeActual.compareTo(timeSiguiente) > 0)
					continue;
				//if(cs.get(i).equals(cs.get(i+1)))
				//	continue;
				date.add(dateActual);
				timeIn.add(timeActual);
				timeOut.add(timeSiguiente);
				ISCorrect.add(IS.get(i));
				i++;
	        }
	        System.out.println(ISCorrect.size());
	        /*for(int i=0;i<ISCorrect.size()/8;i++){
	        	System.out.println(
	        			ISCorrect.get(i) + " " + date.get(i) + " " + timeIn.get(i) + " " + timeOut.get(i)
				);
			}*/
		} catch (EncryptedDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
