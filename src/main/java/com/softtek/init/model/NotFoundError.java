package com.softtek.init.model;

public class NotFoundError extends RuntimeException {

    public NotFoundError(String message) {
        super(message);
    }

}