package com.softtek.init.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Student {
	@Id
	private int idstudent;
	private String name;
	private String time_entrada;
	private String time_salida;
	private String fecha;
	
	public Student() {}

	public Student(int idstudent, String name, String time_entrada, String time_salida, String fecha) {
		this.idstudent = idstudent;
		this.name = name;
		this.time_entrada = time_entrada;
		this.time_salida = time_salida;
		this.fecha = fecha;
	}

	public int getIdstudent() {
		return idstudent;
	}

	public void setIdstudent(int idstudent) {
		this.idstudent = idstudent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime_entrada() {
		return time_entrada;
	}

	public void setTime_entrada(String time_entrada) {
		this.time_entrada = time_entrada;
	}

	public String getTime_salida() {
		return time_salida;
	}

	public void setTime_salida(String time_salida) {
		this.time_salida = time_salida;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Student{" +
				"idstudent=" + idstudent +
				", name='" + name + '\'' +
				", time_entrada='" + time_entrada + '\'' +
				", time_salida='" + time_salida + '\'' +
				", fecha='" + fecha + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Student student = (Student) o;
		return idstudent == student.idstudent &&
				Objects.equals(name, student.name) &&
				Objects.equals(time_entrada, student.time_entrada) &&
				Objects.equals(time_salida, student.time_salida) &&
				Objects.equals(fecha, student.fecha);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idstudent, name, time_entrada, time_salida, fecha);
	}
}
