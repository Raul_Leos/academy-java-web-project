package com.softtek.init.model;

public class PostStudent {
    private String is;
    private String startDate;
    private String endDate;

    public PostStudent(){}

    public PostStudent(String is, String startDate, String endDate) {
        this.is = is;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getIs() {
        return is;
    }

    public void setIs(String is) {
        this.is = is;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "PostStudent{" +
                "is='" + is + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
