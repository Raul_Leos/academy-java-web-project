package com.softtek.init.model;

public class ErrorModel {
    Integer errorCode;
    String exceptionMessage;

    public ErrorModel(){}

    public ErrorModel(Integer errorCode, String exceptionMessage) {
        this.errorCode = errorCode;
        this.exceptionMessage = exceptionMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
