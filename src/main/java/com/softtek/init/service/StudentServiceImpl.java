package com.softtek.init.service;

import com.softtek.init.model.Student;
import com.softtek.init.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> hoursPerMonthPerUser(String is, String month) {
        List<Student> students = studentRepository.getAllByNameAndFecha(is,month);
        return students;
    }

    @Override
    public List<Student> getUserByEndAndStartDates(String is, String startDate, String endDate) {
        List<Student> students = studentRepository.getUserByEndAndStartDates(is,startDate,endDate);
        return students;
    }

    @Override
    public List<Student> getAllByMonth(String mes) {
        return studentRepository.getAllByMonth(mes);
    }

    @Override
    public List<Student> getAllBetweenDates(String startDate, String endDate) {
        return studentRepository.getAllBetweenDates(startDate,endDate);
    }
}
