package com.softtek.init.service;

import com.softtek.init.model.Student;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentService {

    List<Student> hoursPerMonthPerUser(String is, String month);
    List<Student> getUserByEndAndStartDates(String is, String startDate, String endDate);
    List<Student> getAllByMonth(String mes);
    List<Student> getAllBetweenDates(String startDate,String endDate);
}
