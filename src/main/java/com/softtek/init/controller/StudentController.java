package com.softtek.init.controller;

import com.softtek.init.model.NotFoundError;
import com.softtek.init.model.PostStudent;
import com.softtek.init.model.Student;
import com.softtek.init.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static java.time.temporal.ChronoUnit.MINUTES;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/users")
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/{is}/{mes}")
	public Float home(@PathVariable String is, @PathVariable String mes) {
        float suma = 0;
        try {
            String mesFormat = "-" + mes.trim() + "-";
            List<Student> students = studentService.hoursPerMonthPerUser(is.trim(), mesFormat);
            if (students.isEmpty()) {
                throw new NotFoundError("No existen usuarios");
            }

            for (Student student : students) {
                LocalTime entrada = LocalTime.parse(student.getTime_entrada());
                LocalTime salida = LocalTime.parse(student.getTime_salida());
                suma += MINUTES.between(entrada, salida);
            }
        }catch (Exception e){
            throw new NotFoundError("Student not found");
        }

        return suma/60;
    }

    @PostMapping("/")
    public List<Student> postUser(@RequestBody PostStudent postStudent){
        List<Student> students = studentService.getUserByEndAndStartDates(
                postStudent.getIs(),postStudent.getStartDate(),postStudent.getEndDate());
        if(students.isEmpty())
            throw new NotFoundError("No students found");
        return students;
    }
}
