package com.softtek.init.controller;

import com.softtek.init.model.NotFoundError;
import com.softtek.init.model.PostPeriod;
import com.softtek.init.model.Student;
import com.softtek.init.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/period")
public class PeriodController {

    @Autowired
    StudentService studentService;

    @GetMapping("{mes}")
    public List<Student> getAllByMonth(@PathVariable String mes){
        String fecha = "-"+mes+"-";
        List<Student> students =  studentService.getAllByMonth(fecha);
        if(students.isEmpty())
            throw new NotFoundError("No students found");
        return students;
    }

    @PostMapping("/")
    public List<Student> getAllBetweenDates(@RequestBody PostPeriod postPeriod){
        List<Student> students = studentService.getAllBetweenDates(postPeriod.getStartDate(),postPeriod.getEndDate());
        if(students.isEmpty())
            throw new NotFoundError("No students found");
        return students;
    }
}
