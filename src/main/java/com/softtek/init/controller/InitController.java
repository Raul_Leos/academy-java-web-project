package com.softtek.init.controller;

import com.softtek.init.model.NotFoundError;
import com.softtek.init.model.Student;
import com.softtek.init.repositories.StudentRepository;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/init")
public class InitController {

    @Autowired
    private StudentRepository studentRepo;


    @GetMapping("/")
    public String init() {
        File file = null;
        try{
            List<Student> students = studentRepo.findAll();
            if(!students.isEmpty()){
                throw new NotFoundError("Archivo ya inicializado");
            }
        }catch(Exception e){

        }
        if (System.getProperty("catalina.base") != null){
            file = new File(System.getProperty("catalina.base") + "/conf/Registros-momentum.xls");
            Workbook workbook;
            try {
                workbook = WorkbookFactory.create(file);

                // Getting the Sheet at index zero
                Sheet sheet = workbook.getSheetAt(0);

                // Create a DataFormatter to format and get each cell's value as String
                DataFormatter dataFormatter = new DataFormatter();
                int r = 0;
                int c = 0;
                List<String> IS = new ArrayList();
                List<String> fecha = new ArrayList();
                List<String> cs = new ArrayList();
                for (Row row : sheet) {
                    c = 0;
                    for (Cell cell : row) {
                        if (r > 0 && c == 2) {
                            IS.add(dataFormatter.formatCellValue(cell));
                        }
                        if (r > 0 && c == 4) {
                            fecha.add(dataFormatter.formatCellValue(cell));
                        }
                        if (r > 0 && c == 6) {
                            cs.add(dataFormatter.formatCellValue(cell));
                        }
                        c++;
                    }
                    r++;
                }
                // Closing the workbook
                workbook.close();
                List<String> ISCorrect = new ArrayList();
                List<String> date = new ArrayList();
                List<String> timeIn = new ArrayList();
                List<String> timeOut = new ArrayList();
                //Aqui tendriamos la lista llena
                for (int i = 0; i < IS.size(); i++) {
                    String actual = fecha.get(i);
                    String anterior;
                    if ((i + 1) < IS.size())
                        anterior = fecha.get(i + 1);
                    else
                        break;
                    String[] dateTimeActual = actual.split(" ");
                    String dateActual = dateTimeActual[0];
                    String timeActual = dateTimeActual[1];


                    String[] dateTimeSiguiente = anterior.split(" ");
                    String dateSiguiente = dateTimeSiguiente[0];
                    String timeSiguiente = dateTimeSiguiente[1];


                    if (!dateActual.equals(dateSiguiente))
                        continue;
                    if (timeActual.compareTo(timeSiguiente) > 0)
                        continue;
                    date.add(dateActual);
                    timeIn.add(timeActual);
                    timeOut.add(timeSiguiente);
                    ISCorrect.add(IS.get(i));
                    i++;
                }

                for (int i = 0; i < ISCorrect.size(); i++) {
                    Student student = new Student();
                    student.setIdstudent(i);
                    student.setName(ISCorrect.get(i));
                    student.setFecha(date.get(i));
                    student.setTime_entrada(timeIn.get(i));
                    student.setTime_salida(timeOut.get(i));
                    studentRepo.save(student);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw new NotFoundError("File not found");
            }
            return "Correcto";
        }
        return "incorreccto";
    }
}
